<h1>MovieDB</h1>
MovieDB is a gorgeous client application for [TMDb](https://www.themoviedb.org) on Android, built using Kotlin.

## Architecture and Tech-stack

* Built on MVVM architecture pattern
* Uses [Android Architecture Components](https://developer.android.com/topic/libraries/architecture/), specifically ViewModel, LiveData and Room.
* Heavily uses the [Epoxy Library](https://github.com/airbnb/epoxy/) from Airbnb
* Has a clean, gorgeous user interface with pretty animations, built using Android Transitions framework, and [Material Components for Android](https://github.com/material-components/material-components-android)
* Heavily uses [RxJava](https://github.com/ReactiveX/RxJava) for network calls, transformations, and database observation.
* Completely offline ready. MovieDB uses [Room](https://developer.android.com/topic/libraries/architecture/room) for managing a local SQLite database, which means that if you have seen some content already while you were online, you won't need an internet connection to see it again. Everything except movie trailers are cached.
* Uses [Retrofit](https://square.github.io/retrofit/) for making API calls.
* Uses [Glide](https://github.com/bumptech/glide) for image loading.
* Built on a Single-Activity Architecture. Every screen in the app is a fragment.

## Features
* Discover Top Rated and Popular movies on TMDb.
* Search for movies
* View movie details like release date, rating, overview, **movie trailer** and cast right inside the app.
* Supports login for TMDb accounts to manage Watchlist and Favourite movies
* View movies in theatres in your region.
* Works offline by caching data into a database.

